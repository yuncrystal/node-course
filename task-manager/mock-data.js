const { ObjectId } = require("mongodb");

const tasks = [
  {
    description: "Study PHP",
    completed: false,
  },
  {
    description: "Study JS",
    completed: false,
  },
  {
    description: "Study Javascript",
    completed: false,
  },
];

const uid = new ObjectId();
// console.log("uid:", uid);
// console.log("uid string:", uid.toHexString().length);
// console.log("uid id:", uid.id);

const user = {
  _id: uid,
  name: "June",
  age: 30,
  sex: "female",
};

module.exports = {
  tasks,
  user,
};

import express, { Request, Response } from "express";

import User, { AllowedUpdateFields } from "../models/user";

const router = express.Router();

router.post("/users", async (req: Request, res: Response) => {
  const user = new User(req.body);
  try {
    await user.save();
    res.send(user);
  } catch (err) {
    res.status(400).send(err);
  }
});

router.get("/users", async (req: Request, res: Response) => {
  try {
    const users = await User.find({});
    res.send(users);
  } catch (err) {
    res.status(500).send();
  }
});
router.get("/users/:id", async (req: Request, res: Response) => {
  const _id = req.params.id;
  try {
    const user = await User.findById(_id);
    if (!user) return res.status(404).send();
    res.send(user);
  } catch (err) {
    res.status(500).send(err);
  }
});

router.patch("/users/:id", async (req: Request, res: Response) => {
  const _id = req.params.id;
  const updatedUser = req.body;
  const updates = Object.keys(updatedUser);
  const isValidOperation = updates.every((update) =>
    AllowedUpdateFields.includes(update)
  );
  if (!isValidOperation)
    return res.status(400).send({
      error: "Invalid updates!",
    });
  try {
    const user = await User.findById(_id);
    if (!user) return res.status(404).send();
    Object.assign(user, updatedUser);
    await user.save();
    res.send(user);
  } catch (err) {
    res.status(500).send(err);
  }
});

export default router;

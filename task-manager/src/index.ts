import express from "express";
import userRouter from "./routers/user";
import "./db/mongoose.js";

const app = express();
app.use(express.json());

app.use(userRouter);

const port = process.env.PORT || 3000;


app.listen(port, () => {
  console.log("Server is up on prot: ", port);
});

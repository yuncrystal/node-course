function insertOne(db, collection, data) {
  db.collection(collection).insertOne(data, (error, result) => {
    if (error) {
      return console.log(`Unable to insert ${collection}`);
    }
    console.log("result:", result);
  });
}

function insertMany(db, collection, array) {
  db.collection(collection).insertMany(array, (error, result) => {
    if (error) {
      return console.log(`Unable to insert ${collection}`);
    }
    console.log("result:", result);
  });
}

function findOne(db, collection, query) {
  db.collection(collection).findOne(query, (error, result) => {
    if (error) {
      return console.log(`Unable to find data in ${collection}`);
    }
    console.log(`find in ${collection}:`, result);
  });
}

function find(db, collection, query) {
  db.collection(collection)
    .find(query)
    .toArray((error, result) => {
      if (error) {
        return console.log(`Unable to find data in ${collection}`);
      }
      console.log(`find in ${collection}:`, result);
    });
}

module.exports = {
  insertOne,
  insertMany,
  findOne,
  find,
};

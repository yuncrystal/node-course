import { Schema, model } from "mongoose";
import { IUser, IUserDoc } from "../types/user";
import bcrypt from "bcryptjs";

export const AllowedUpdateFields = [
  "name",
  "email",
  "age",
  "password",
  "nickname",
];

export const UserSchemaFields: Record<keyof IUser, object> = {
  name: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    trim: true,
    required: true,
  },
  age: {
    type: Number,
    required: true,
  },
  password: {
    type: String,
    required: true,
    trim: true,
  },
  nickname: String,
};

const userSchema = new Schema(UserSchemaFields);

const User = model<IUserDoc>("User", userSchema);

const salt = bcrypt.genSaltSync(10);

// statics: add query helper functions, which are like instance methods
userSchema.statics.findByCredentials = async (email, password) => {
  const user = await User.findOne({ email });
  if (!user) {
    throw new Error("Unable to login");
  }
  // const isMatch = bcrypt.compare(password, user.password);
};

// Hash the plain password before save/update the user
userSchema.pre("save", function (next) {
  const user = this as IUserDoc;
  if (user.isModified("password")) {
    user.password = bcrypt.hashSync(user.password, salt);
  }
  next();
});

export default User;

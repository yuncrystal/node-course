import { Document } from "mongoose";

export interface IUser {
  name: string;
  email: string;
  age: number;
  password: string;
  nickname?: string;
}

export interface IUserDoc extends IUser, Document {}



//CRUD create read  update delete

const { MongoClient, ObjectId } = require("mongodb");
const { insertOne, insertMany, findOne, find } = require("../controller/CRUD");
const { tasks, user } = require("../../mock-data");
const assert = require("assert");

// Connection URL
const connectionURL = "mongodb://127.0.0.1:27017";

// Database Name
const dbName = "task-manager";
const collectionUser = "user";
const collectionTask = "tasks";
// Use connect method to connect to the server
MongoClient.connect(connectionURL, function (err, client) {
  assert.equal(null, err);
  console.log("Connected successfully to server");

  const db = client.db(dbName);
  // insertOne(db, collectionUser, user);
  // insertMany(db, collectionTask, tasks);

  findOne(db, collectionTask, { completed: false });
  find(db, collectionTask, { completed: false });
  find(db, collectionTask, { _id: new ObjectId("605740425f2c9f1eb32d6f5d") });

  client.close();
});

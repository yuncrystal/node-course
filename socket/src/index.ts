import express from "express";
import { createServer } from "http";
import { Socket } from "socket.io";
// import { mock } from "./mock/ohrc";
// import { connectToKafka } from "./kafka";
const app = express();
const server = createServer(app);

interface ISubscribedRooms {
  [props: string]: number;
}
interface ISocketRooms {
  [props: string]: boolean;
}

const subscribedRooms: ISubscribedRooms = {};

const io = require("socket.io")(server, {
  cors: {
    origin: ["http://localhost:8080"],
    methods: ["GET", "POST"],
  },
});

io.on("connection", (socket: Socket) => {
  const socketRooms: ISocketRooms = {};
  socket.on("subscribe", (room: string) => {
    if (!socketRooms[room]) {
      subscribedRooms[room] = subscribedRooms[room]
        ? subscribedRooms[room] + 1
        : 1;
      socketRooms[room] = true;
      socket.join(room);
    }
  });

  socket.on("unsubscribe", (room: string) => {
    if (socketRooms[room]) {
      socket.leave(room);
      socketRooms[room] = false;
      subscribedRooms[room] = subscribedRooms[room] - 1;
    }
  });

  socket.on("disconnect", () => {
    Object.keys(socketRooms).forEach((key) => {
      if (socketRooms[key]) {
        subscribedRooms[key] = subscribedRooms[key] - 1;
        delete socketRooms[key];
      }
    });
    console.log("subscribedRooms:", subscribedRooms);
  });
});

let obj = {
  u: 1618362660,
  v: 1778,
  o: 1.08222,
  h: 1.08235,
  l: 1.082,
  c: 1.08201,
  lc: 0.0,
  a: 1.08222,
};

setInterval(() => {
  if (subscribedRooms["room1"]) {
    for (let i = 0; i < 10; i++) {
      io.to("room1").to("All").emit("room1", JSON.stringify(obj));
      io.to("room2").to("All").emit("room2", JSON.stringify(obj));
    }
  }
}, 1000);

// io.of("/").adapter.on("create-room", (room: any) => {
//   console.log(`room ${room} was created`);
// });

// io.of("/").adapter.on("join-room", (room: any, id: any) => {
//   console.log(`socket ${id} has joined room ${room}`);
// });

// app.get("/", (req, res) => {
//   res.sendFile(__dirname + "/index.html");
// });

server.listen(process.env.PORT, () => {
  console.log("listening on *:", process.env.PORT);
});

import { Kafka, logLevel } from "kafkajs";
import { getChannels } from "./channels";

function connectToKafka(callback: any) {
  const kafka = new Kafka({
    clientId: "kafka-socket",
    brokers: [
      <string>process.env.BROKER_HOST_1,
      <string>process.env.BROKER_HOST_2,
    ],
    connectionTimeout: 3000,
    logLevel: logLevel.ERROR,
    retry: {
      initialRetryTime: 100,
      retries: 5,
    },
  });

  const topicReg = /1-[a-z]+/i;
  const consumer = kafka.consumer({ groupId: "group-c" });

  const run = async () => {
    const channels = await getChannels();
    await consumer.connect();
    await consumer.subscribe({ topic: topicReg, fromBeginning: false });
    console.log("connect successful");

    await consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
        let value = message.value?.toString();
        let symbolName = topic.split("-")[1];
        callback(channels[symbolName], value);
      },
    });
  };

  run().catch((e) => console.error(`errorMessage ${e.message}`, e));
}

export { connectToKafka };

export interface ISymbolChannelMap {
  [props: string]: string;
}

interface IChannel {
  source_channel: string;
  target_channel: string;
}

export type IChannels = IChannel[];

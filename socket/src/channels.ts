import { ISymbolChannelMap, IChannels } from "./types/channel";
import fetch from "node-fetch";
const symbolChannelMap: ISymbolChannelMap = {};

async function getChannels() {
  try {
    const res = await fetch(
      process.env.API_BRIDGE_URL +
        "/v1/inner/channels?parameters=source_channel,target_channel"
    );
    const channels: IChannels = await res.json();
    for (let i = 0; i < channels.length; i++) {
      const sourceChannel = channels[i].source_channel;
      symbolChannelMap[sourceChannel] = channels[i].target_channel;
    }
  } catch (err) {
    console.log("err:", err);
  }

  return symbolChannelMap;
}

export { getChannels };

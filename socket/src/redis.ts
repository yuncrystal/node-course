import Redis from "ioredis";

function connectToStaticRedis() {
  let static_redis = new Redis({
    port: parseInt(<string>process.env.REDIS_PORT),
    host: process.env.REDIS_HOST,
    family: parseInt(<string>process.env.REDIS_FAMILY),
    password: process.env.REDIS_PASSWORD,
    db: parseInt(<string>process.env.REDIS_DB),
  });

  static_redis.subscribe("1-2-1", function (err, count) {
    if (err) {
      // Just like other commands, subscribe() can fail for some reasons,
      // ex network issues.
      console.error("Failed to subscribe: %s", err.message);
    } else {
      // `count` represents the number of channels this client are currently subscribed to.
      console.log(
        `Subscribed successfully! This client is currently subscribed to ${count} channels.`
      );
    }
  });

  static_redis.on("message", function (channel, message) {
    console.log("channel:", channel, message);
  });

  return static_redis;
}

export { connectToStaticRedis };

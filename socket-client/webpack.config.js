const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require("webpack");

module.exports = () => {
  return {
    mode: "development",
    entry: "./src/index.ts",
    devServer: {
      historyApiFallback: true,
      contentBase: path.resolve(__dirname, "./dist"),
      open: false,
      compress: true,
      hot: true,
      port: 8080,
    },
    resolve: {
      extensions: [".ts", ".js"],
    },
    output: {
      path: path.resolve(__dirname, "./dist"),
      filename: "[name].bundle.js",
    },
    module: {
      rules: [
        {
          test: /\.jsx?|.tsx?|.js?|.ts?$/,
          exclude: /(node_modules)/,
          use: {
            loader: "babel-loader",
          },
        },

        {
          test: /\.tsx?|.ts?$/,
          exclude: /(node_modules)/,
          use: {
            loader: "ts-loader",
          },
        },
      ],
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebpackPlugin({
        title: "webpack Boilerplate",
        template: path.resolve(__dirname, "./src/index.html"), // template file
        filename: "index.html", // output file
      }),
    ],
  };
};

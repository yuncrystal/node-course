import io from "socket.io-client";

// const socket = io("https://bridgetest.finlogixdev.com", {
//   transports: ["websocket"],
// });

const socket = io("localhost:3000", {
  transports: ["websocket"],
});

// const socket: SocketIOClient.Socket = io("localhost:3000", {
//   transports: ["websocket"],
// });

const subscribedRooms: string[] = [];

const subscribeButton = document.getElementById(
  "subscribe"
) as HTMLButtonElement;
const input = document.getElementById("input") as HTMLInputElement;
const ul = document.getElementById("list") as HTMLUListElement;

const handleUnsubscribe = (
  room: string,
  socket: SocketIOClient.Socket,
  unsubscribeBtn: HTMLButtonElement
) => {
  socket.emit("unsubscribe", room);
  socket.off(room);
  subscribedRooms.splice(subscribedRooms.indexOf(room), 1);
  unsubscribeBtn.parentNode?.parentNode?.removeChild(unsubscribeBtn.parentNode);
};

subscribeButton?.addEventListener("click", () => {
  const room = input.value;
  if (!subscribedRooms.includes(room)) {
    subscribedRooms.push(room);
    socket.emit("subscribe", room);

    //@ts-ignore
    socket.onAny((event: string, ...msg: any) => {
      console.log("event:", event, msg);
    });

    const li = document.createElement("li");
    li.appendChild(document.createTextNode(room));

    const unsubscribeBtn = document.createElement(
      "button"
    ) as HTMLButtonElement;

    unsubscribeBtn.innerHTML = "unsubscribe";
    unsubscribeBtn.addEventListener("click", () =>
      handleUnsubscribe(room, socket, unsubscribeBtn)
    );

    li.appendChild(unsubscribeBtn);
    ul.appendChild(li);
  }
});

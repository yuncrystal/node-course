// 从命令行输入： node app.js add --title='My first notes'


const yargs = require('yargs');

// console.log(process.argv[3])


// add, remove, read, list

yargs.command({
    command:'add',
    describe:'Add a new note',
    builder:{
        title:{
            describe:'Note title',
            demandOption:true,
            type:'string'
        }
    },
    handler:function(argv){
        console.log('Add a new note:',argv.title);
    }
});

yargs.command({
    command:'remove',
    describe:'Remove a note',
    handler:function(){
        console.log('Remove a new note');
    }
});

yargs.command({
    command:'read',
    describe:'Read a note',
    handler:function(){
        console.log('Read a new note');
    }
});

yargs.command({
    command:'list',
    describe:'List a note',
    handler:function(){
        console.log('List a new note');
    }
})

// this line should put after the yargs.command lines, 
// otherwise it will not show error message
// console.log(yargs.argv) same meaning with the following line
yargs.parse();

